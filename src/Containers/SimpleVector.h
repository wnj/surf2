/* $BSD_LICENSE_HEADER_START */
/*
 * Copyright 2013, 2014 Li-Yuchong(李雨翀) <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $BSD_LICENSE_HEADER_END */

#ifndef __SIMPLE_VECTOR_H__
#define __SIMPLE_VECTOR_H__

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <assert.h>

#ifndef MALLOC
#   define MALLOC(_size) malloc(_size)
#endif /* MALLOC */

#ifndef REALLOC
#   define REALLOC(_ptr, _size) realloc(_ptr, _size)
#endif /* REALLOC */

#ifndef MFREE
#   define MFREE(_ptr) free(_ptr)
#endif /* MFREE */

#ifndef MEMCPY
#   define MEMCPY memcpy
#endif /* MEMCPY */

#ifndef ASSERT
#   define ASSERT assert
#endif /* ASSERT */


#define SV_DECLARE(__vector_type, type)                                        \
    SIMPLE_VECTOR_DECLARE(__vector_type, type)

#define SV_SIZE(__vector_in)                                                   \
    SIMPLE_VECTOR_SIZE(__vector_in)

#define SV_AT(__vertor_in, __element_index)                                    \
    SIMPLE_VECTOR_AT(__vertor_in, __element_index)

#define SV_PUSH_BACK(__vector_in, __data_in, __type_name)                      \
    SIMPLE_VECTOR_PUSH_BACK(__vector_in, __data_in, __type_name)

#define SV_ALLOC(__vector_type, __vector_in, __init_size, __type_name)         \
    SIMPLE_VECTOR_ALLOC(__vector_type, __vector_in, __init_size, __type_name)

#define SV_RELEASE(__vector_in)                                                \
    SIMPLE_VECTOR_RELEASE(__vector_in)

#define SV_INIT(__vector_in, __init_size, __type_name)                         \
    SIMPLE_VECTOR_INIT(__vector_in, __init_size, __type_name)

#define SV_S_AT(__vertor_in, __element_index)                                  \
    SIMPLE_VECTOR_STACK_AT(__vertor_in, __element_index)

#ifndef __SIMPLE_VECTOR_MALLOC_CHECK
#define __SIMPLE_VECTOR_MALLOC_CHECK(__ptr_in)                                 \
do {                                                                           \
    if(!(__ptr_in))                                                            \
    {                                                                          \
        fprintf(stderr, "fatal: memory allocation faild in function %s(), "    \
                        "file: %s, line: %d. exit.\n",                         \
                __FUNCTION__, __FILE__, __LINE__);                             \
        exit(1);                                                               \
    }                                                                          \
} while(0)
#endif


#define SIMPLE_VECTOR_DECLARE(__vector_type, type)                             \
struct __vector_type                                                           \
{                                                                              \
    type *data;                                                                \
    size_t size;                                                               \
                                                                               \
    size_t __allocatedLength;                                                  \
} typedef __vector_type                                                        \



#define SIMPLE_VECTOR_ALLOC(__vector_type, __vector_in,                        \
                            __init_size, __type_name)                          \
do {                                                                           \
    size_t __size_ = __init_size;                                              \
    if(__size_ == 0)                                                           \
        __size_ = 2;                                                           \
    __vector_type *__vector_ =                                                 \
        ( __vector_type *)MALLOC(sizeof(__vector_type));                       \
    __SIMPLE_VECTOR_MALLOC_CHECK(__vector_);                                   \
                                                                               \
    __vector_->data =                                                          \
        (__type_name *)MALLOC(sizeof(__type_name) * (__size_));                \
    __SIMPLE_VECTOR_MALLOC_CHECK(__vector_->data);                             \
                                                                               \
    __vector_->__allocatedLength = (__size_);                                  \
    __vector_->size = 0;                                                       \
                                                                               \
    (__vector_in) = __vector_;                                                 \
} while(0)

#define SIMPLE_VECTOR_INIT(__vector_in, __init_size, __type_name)              \
do {                                                                           \
    assert(__vector_in);                                                       \
    size_t __size_ = __init_size;                                              \
    if(__size_ == 0)                                                           \
        __size_ = 2;                                                           \
    (__vector_in)->data =                                                      \
    (__type_name *)MALLOC(sizeof(__type_name) * (__size_));                    \
        __SIMPLE_VECTOR_MALLOC_CHECK((__vector_in)->data);                     \
                                                                               \
    (__vector_in)->__allocatedLength = (__size_);                              \
    (__vector_in)->size = 0;                                                   \
} while(0)


#define SIMPLE_VECTOR_PUSH_BACK(__vector_in, __data_in, __type_name)           \
do {                                                                           \
    if((__vector_in)->__allocatedLength - 1 < (__vector_in)->size)             \
    {                                                                          \
        (__vector_in)->__allocatedLength *= 2;                                 \
                                                                               \
        __type_name *__realloced_address_ =                                    \
            (__type_name *)REALLOC((__vector_in)->data,                        \
            sizeof(__type_name) * (__vector_in)->__allocatedLength);           \
                                                                               \
            __SIMPLE_VECTOR_MALLOC_CHECK(__realloced_address_);                \
                                                                               \
        (__vector_in)->data = __realloced_address_;                            \
    }                                                                          \
                                                                               \
    (__vector_in)->data[(__vector_in)->size] = (__data_in);                    \
    (__vector_in)->size++;                                                     \
                                                                               \
} while(0)


#define SIMPLE_VECTOR_ERASE_AT(__vector, __element_index, __type_name)         \
do {                                                                           \
    if((__element_index > (__vector)->size - 1) || (__element_index) < 0)      \
        ASSERT(!"fatal: SIMPLE_VECTOR: out of range.");                        \
                                                                               \
    MEMCPY((__vector)->data + (__element_index),                               \
           (__vector)->data + (__element_index) + 1,                           \
           sizeof(__type_name) * ((__vector)->size - (__element_index) - 1));  \
    (__vector)->size--;                                                        \
} while(0)


#define SIMPLE_VECTOR_RELEASE(__vector_in)                                     \
do {                                                                           \
    MFREE((__vector_in)->data);                                                \
    (__vector_in)->data = NULL;                                                \
    MFREE((__vector_in));                                                      \
    (__vector_in) = NULL;                                                      \
} while(0)


#define SIMPLE_VECTOR_AT(__vertor_in, __element_index)                         \
    ((__vertor_in)->data[__element_index])


#define SIMPLE_VECTOR_SIZE(__vector_in)                                        \
    ((__vector_in)->size)

#endif /* __SIMPLE_VECTOR_H__ */
