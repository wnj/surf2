/*
 * Copyright 2013, 2014 Li-Yuchong <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <time.h>

#include "ImageIO.h"
#include "Painters.h"
#include "SURF.h"

#define PARSING_ERROR                                          \
do {                                                           \
    fprintf(stderr, "error while parsing arguments. exit.\n"); \
    exit(1);                                                   \
} while(0)

int main(int argc, const char *argv[])
{
    char  *fileName;
    char  *outputFile;
    float orientation, scale;
    int   i;
    
    float threshold;
    
    Image               *image;
    FeaturePoint        *featurePoint;
    FeaturePointsVector *features;

    threshold = 0.001f;

    
    /* parse input arguments */
    if(argc != 5 && argc != 7)
    {
        fprintf(stderr, "usage: %s \n       "
                "-i | --input <path-to-input> -o | --output <path-to-output>\n"
                "[-t | --threshold <value>]", argv[0]);
        exit(1);
    }
    
    for(i = 1; i < argc; i++)
    {
        if(strcmp(argv[i], "--input") == 0 || strcmp(argv[i], "-i") == 0)
        {
            fileName = (char *)argv[i + 1];
            i++;
        }
        else if(strcmp(argv[i], "--output") == 0 || strcmp(argv[i], "-o") == 0)
        {
            outputFile = (char *)argv[i + 1];
            i++;
        }
        else if(strcmp(argv[i], "--threshold") == 0 || strcmp(argv[i], "-t") == 0)
        {
            threshold = atof(argv[i + 1]);
            i++;
        }
        else
        {
            fprintf(stderr, "unknown argument: %s\n", argv[i]);
            PARSING_ERROR;
        }
    }
    
    /* decode the image and save to the variable image */
    image = decodeFromFile(fileName);
    fprintf(stdout, "ImageIO: image size %d * %d\n", image->imageWidth, image->imageHeight);
    
    clock_t start = clock();
    
    /* detect SURF */
    features = SURF(image, threshold, DEFAULT_OCTAVE_FILTER_COUNT, DEFAULT_INITIAL_SAMPLE_COUNT);
    fprintf(stdout, "SURF   : %ld features extracted in %f seconds.\n",
            SIMPLE_VECTOR_SIZE(features), (float)(clock() - start) / CLOCKS_PER_SEC);

    /* draw circles and lines */
    for(i = 0; i < SIMPLE_VECTOR_SIZE(features); i++)
    {
        featurePoint = SIMPLE_VECTOR_AT(features, i);
        scale        = featurePoint->scale * 5;
        orientation  = featurePoint->orientation;
        
        drawCircle(image, featurePoint->x, featurePoint->y, scale, COLOR_RED);
        drawLine(image, featurePoint->x, featurePoint->y,
                             sin(orientation) * scale + featurePoint->x,
                             cos(orientation) * scale + featurePoint->y, COLOR_YELLOW);
    }
    
    /* save the output image */
    encodeToFileFromImage(outputFile, image, IMAGE_FORMAT_TIFF);
    
    /* clean up */
    imageRelease(image);
    for(i = 0; i < SIMPLE_VECTOR_SIZE(features); i++)
        releaseFeaturePoint(SIMPLE_VECTOR_AT(features, i));
    SIMPLE_VECTOR_RELEASE(features);
    
    return 0;
}
