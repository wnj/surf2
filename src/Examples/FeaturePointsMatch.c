/*
 * Copyright 2013, 2014 Li-Yuchong <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>

#include "ImageIO.h"
#include "SURF.h"
#include "Matchers.h"
#include "Painters.h"

#include "Memory.h"

#define __MAX(a, b) ((a) > (b) ? (a) : (b))
void matchResultVisualize(Image *imageA, Image *imageB, MatchPointsVector *matches, char *outputImage)
{
    int    resultWidth, resultHeight;
    Image  matchResult;
    Match  match;
    size_t i;
    
    /* calculate the size of the output image */
    resultWidth = imageB->imageWidth + imageA->imageWidth;
    resultHeight = __MAX(imageA->imageHeight, imageB->imageHeight);
    
    /* alloc memory for the output image */
    initializeImageWithSize(&matchResult, resultWidth, resultHeight);
    
    /* paint the image A & B to the output image */
    paintImageTo(imageB, &matchResult, 0, 0);
    paintImageTo(imageA, &matchResult, imageB->imageWidth, 0);
    
    /* draw the feature points and connect them */
    for(i = 0; i < SIMPLE_VECTOR_SIZE(matches); i++)
    {
        match = SIMPLE_VECTOR_AT(matches, i);

        drawLine(&matchResult,
                 match.featurePointA->x, match.featurePointA->y,
                 match.featurePointB->x + imageB->imageWidth,
                 match.featurePointB->y, COLOR_YELLOW);
        
        drawCircle(&matchResult, match.featurePointA->x,
                   match.featurePointA->y, 5, COLOR_RED);
        drawCircle(&matchResult, match.featurePointB->x + imageB->imageWidth,
                   match.featurePointB->y, 5, COLOR_RED);
    }
    
    /* save the output image */
    encodeToFileFromImage(outputImage, &matchResult, IMAGE_FORMAT_TIFF);
    
    /* clean up */
    free(matchResult.pixel);
}
#undef __MAX

void printMatches(MatchPointsVector *matches)
{
    size_t i;
    Match match;
    
    /* paint feature points on the image A & B */
    for(i = 0; i < SIMPLE_VECTOR_SIZE(matches); i++)
    {
        match = SIMPLE_VECTOR_AT(matches, i);
        
        fprintf(stdout, "[%.2f, %.2f] -> [%.2f, %.2f]\n",
                match.featurePointA->x, match.featurePointA->y,
                match.featurePointB->x, match.featurePointB->y);
    }
}

int ____main(int argc, char *argv[])
{
    int i;
    
    char  *fileNameA = "/users/nt/desktop/test/NTC_1795.JPG";
    char  *fileNameB = "/users/nt/desktop/test/NTC_1796.JPG";
    
    float threshold;
    
    Image               *imageA, *imageB;
    FeaturePointsVector *featuresA, *featuresB;
    MatchPointsVector   *matches;
    
    threshold = 0.00001f;
    
    /* decode the images */
    imageA = decodeFromFile(fileNameA);
    imageB = decodeFromFile(fileNameB);

    /* detect SURF for image A */
    featuresA = SURF(imageA, threshold, DEFAULT_OCTAVE_FILTER_COUNT, DEFAULT_INITIAL_SAMPLE_COUNT);
    fprintf(stdout, "SURF: image A: %ld features extracted.\n", SIMPLE_VECTOR_SIZE(featuresA));
    
    /* detect SURF for image B */
    featuresB = SURF(imageB, threshold, DEFAULT_OCTAVE_FILTER_COUNT, DEFAULT_INITIAL_SAMPLE_COUNT);
    fprintf(stdout, "SURF: image B: %ld features extracted.\n", SIMPLE_VECTOR_SIZE(featuresB));
    
    /* match the teature points */
    matches = NearestNeighborMatch(featuresA, featuresB);

    /* print the match results to stdout */
    printMatches(matches);
    
    fprintf(stdout, "NearestNeighborMatcher: %ld Matches\n", SIMPLE_VECTOR_SIZE(matches));
    
    /* visualize the match result */
    matchResultVisualize(imageA, imageB, matches, "/tmpfs/match.tiff");
    
    /* clean up */
    
    for(i = 0; i < SIMPLE_VECTOR_SIZE(featuresA); i++)
        releaseFeaturePoint(SIMPLE_VECTOR_AT(featuresA, i));
    for(i = 0; i < SIMPLE_VECTOR_SIZE(featuresB); i++)
        releaseFeaturePoint(SIMPLE_VECTOR_AT(featuresB, i));
    
    SIMPLE_VECTOR_RELEASE(matches);
    SIMPLE_VECTOR_RELEASE(featuresA);
    SIMPLE_VECTOR_RELEASE(featuresB);

    imageRelease(imageA);
    imageRelease(imageB);
    
    return 0;
}
