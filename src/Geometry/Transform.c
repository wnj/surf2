/*
 * Copyright 2013, 2014 Li-Yuchong <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <assert.h>
#include <string.h>

#include "Image.h"
#include "Memory.h"

void applyPointTransform(const float x, const float y,
                         float *ox, float *oy, const double *H)
{
    double xPrime, yPrime, wPrime;
    
    xPrime = H[0] * x + H[1] * y + H[2];
    yPrime = H[3] * x + H[4] * y + H[5];
    wPrime = H[6] * x + H[7] * y + H[8];
    
    xPrime /= wPrime;
    yPrime /= wPrime;
    
    *ox = xPrime,
    *ox = yPrime;
}

void transform(const Image *image, Image *result, double *H)
{
    int i, j, x, y;
    int tmpIndex;
    
    uint8_t r1, g1, b1, a1, r2, g2, b2, a2, r3, g3, b3, a3, r4, g4, b4, a4;
    uint8_t r, g, b, a;
    
    double xPrime, yPrime, wPrime, u, v, xv, yv;
    
    /* find the inverse matrix of matrix H */
    /* pre-evaluation */
    double denominator = -H[2] * H[4] * H[6] + H[1] * H[5] * H[6] +
                          H[2] * H[3] * H[7] - H[0] * H[5] * H[7] -
                          H[1] * H[3] * H[8] + H[0] * H[4] * H[8];
    double Hi[] =
    {
        ((-H[5]) * H[7] + H[4] * H[8]) / denominator,
        (H[2] * H[7] - H[1] * H[8])    / denominator,
        ((-H[2]) * H[4] + H[1] * H[5]) / denominator,
        (H[5] * H[6] - H[3] * H[8])    / denominator,
        ((-H[2]) * H[6] + H[0] * H[8]) / denominator,
        (H[2] * H[3] - H[0] * H[5])    / denominator,
        ((-H[4]) * H[6] + H[3] * H[7]) / denominator,
        (H[1] * H[6] - H[0] * H[7])    / denominator,
        ((-H[1]) * H[3] + H[0] * H[4]) / denominator
    };
    
    int bytesPerLine = image->imageWidth * 4;
    int bytesPerLineTarget = result->imageWidth * 4;
    
    assert(image && result && H);
    
    for(i = 0; i < image->imageWidth; i++)
    {
        for(j = 0; j < image->imageHeight; j++)
        {
            xPrime = Hi[0] * i + Hi[1] * j + Hi[2];
            yPrime = Hi[3] * i + Hi[4] * j + Hi[5];
            wPrime = Hi[6] * i + Hi[7] * j + Hi[8];
            
            xPrime /= wPrime;
            yPrime /= wPrime;
            
            x = (int)xPrime, y = (int)yPrime;
            
            if (x < (image->imageWidth - 1) && y
                < (image->imageHeight - 1) && x > 0 && y > 0)
            {
                xv = (int)xPrime;        yv = (int)yPrime;
                u = xPrime - (double)xv; v = yPrime - (double)yv;
                
                tmpIndex = bytesPerLine * y + x * 4;
                r1 = image->pixel[tmpIndex];
                g1 = image->pixel[tmpIndex + 1];
                b1 = image->pixel[tmpIndex + 2];
                a1 = image->pixel[tmpIndex + 3];
                
                tmpIndex = bytesPerLine * (y + 1) + x * 4;
                r2 = image->pixel[tmpIndex];
                g2 = image->pixel[tmpIndex + 1];
                b2 = image->pixel[tmpIndex + 2];
                a2 = image->pixel[tmpIndex + 3];
                
                tmpIndex = bytesPerLine * y + (x + 1) * 4;
                r3 = image->pixel[tmpIndex];
                g3 = image->pixel[tmpIndex + 1];
                b3 = image->pixel[tmpIndex + 2];
                a3 = image->pixel[tmpIndex + 3];
                
                tmpIndex = bytesPerLine * (y + 1) + (x + 1) * 4;
                r4 = image->pixel[tmpIndex];
                g4 = image->pixel[tmpIndex + 1];
                b4 = image->pixel[tmpIndex + 2];
                a4 = image->pixel[tmpIndex + 3];
                
                r = (uint8_t)((1.0f - u) * (1.0f - v) * r1 + (1.0f - u) * v
                              * r2 + u * (1.0f - v) * r3 + u * v * r4);
                g = (uint8_t)((1.0f - u) * (1.0f - v) * g1 + (1.0f - u) * v
                              * g2 + u * (1.0f - v) * g3 + u * v * g4);
                b = (uint8_t)((1.0f - u) * (1.0f - v) * b1 + (1.0f - u) * v
                              * b2 + u * (1.0f - v) * b3 + u * v * b4);
                a = (uint8_t)((1.0f - u) * (1.0f - v) * a1 + (1.0f - u) * v
                              * a2 + u * (1.0f - v) * a3 + u * v * a4);
                
                tmpIndex = bytesPerLineTarget * j + i * 4;
                result->pixel[tmpIndex] = r;
                result->pixel[tmpIndex + 1] = g;
                result->pixel[tmpIndex + 2] = b;
                result->pixel[tmpIndex + 3] = a;
            }
        }
    }
}
