/*
 * Copyright 2013, 2014 Li-Yuchong <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <assert.h>
#include <stdio.h>

#include "Painters.h"
#include "Memory.h"

void paintImageTo(const Image *image, Image *target, int x, int y)
{
    if(!target->pixel)
    {
        target->pixel =
        (BYTE *)malloc(sizeof(BYTE) * (x + image->imageWidth) *
                                      (y + image->imageHeight) * 4);
        MALLOC_FAIL_CHECK(target->pixel);
        
        target->imageWidth = (x + image->imageWidth);
        target->imageHeight = (y + image->imageHeight);
    }
    
    int bytesPerLineTgt = target->imageWidth * 4;
    int bytesPerLineSrc = image->imageWidth * 4;
    
    for(int i = 0; i < image->imageWidth; i++)
    {
        for(int j = 0; j < image->imageHeight; j++)
        {
            if((j + y) <= target->imageHeight && (j + y) >= 0 && (i + x)
               <= target->imageWidth && (i + x) >= 0)
            {
                target->pixel[bytesPerLineTgt * (j + y) + (i + x) * 4] =
                image->pixel[bytesPerLineSrc * j + i * 4];
                target->pixel[bytesPerLineTgt * (j + y) + (i + x) * 4 + 1] =
                image->pixel[bytesPerLineSrc * j + i * 4 + 1];
                target->pixel[bytesPerLineTgt * (j + y) + (i + x) * 4 + 2] =
                image->pixel[bytesPerLineSrc * j + i * 4 + 2];
                target->pixel[bytesPerLineTgt * (j + y) + (i + x) * 4 + 3] =
                image->pixel[bytesPerLineSrc * j + i * 4 + 3];
            }
        }
    }
}

void drawLine(Image *image, int x0, int y0, int x1, int y1,
                          int color)
{
    /* swap */
    if (y0 > y1)
    {
        int temp = y0; y0 = y1; y1 = temp;
        temp = x0; x0 = x1; x1 = temp;
    }
    
    int bytesPerLine = image->imageWidth * 4;
    
    SET_IMAGE_PIXEL(x0, y0, color);

    int dirX, deltaX = x1 - x0;
    if (deltaX >= 0)
       dirX = 1;
    else
    {
       dirX   = -1;
       deltaX = 0 - deltaX;
    }

    int deltaY = y1 - y0;
    if (deltaY == 0)
    {
        while (deltaX-- != 0)
        {
            x0 += dirX;
            SET_IMAGE_PIXEL(x0, y0, color);
        }
        return;
    }
    if (deltaX == 0)
    {
        do
        {
            y0++;
            SET_IMAGE_PIXEL(x0, y0, color);
        } while (--deltaY != 0);
        return;
    }
    
    if (deltaX == deltaY)
    {
        /* Diagonal line */
        do
        {
            x0 += dirX;
            y0++;
            SET_IMAGE_PIXEL(x0, y0, color);
        } while (--deltaY != 0);
        return;
    }
    
    unsigned short error;
    unsigned short accumulatedErrorTemp, weighting;
    

    unsigned short accumulatedError = 0;
    
    unsigned char rl = GETR(color);
    unsigned char gl = GETG(color);
    unsigned char bl = GETB(color);
    double grayl = rl * 0.299f + gl * 0.587f + bl * 0.114f;
    

    if (deltaY > deltaX)
    {
        error = ((unsigned long) deltaX << 16) / (unsigned long) deltaY;

        while (deltaY--)
        {
            accumulatedErrorTemp = accumulatedError;
            accumulatedError += error;
            
            if (accumulatedError <= accumulatedErrorTemp)
                x0 += dirX;
            
            y0++;
            
            weighting = accumulatedError >> 8;
            assert(weighting < 256);
            assert((weighting ^ 255) < 256);
            
            int backgroundColor = GET_IMAGE_PIXEL(x0, y0);
            
            BYTE rb = GETR(backgroundColor);
            BYTE gb = GETG(backgroundColor);
            BYTE bb = GETB(backgroundColor);
            
            float grayscale = rb * 0.299f + gb * 0.587f + bb * 0.114f;
            
            BYTE rr = (rb >rl ? ((BYTE)(((float)(grayl < grayscale ?
                            weighting : (weighting ^ 255)))
                      / 255.0 * (rb - rl) + rl)) :
                       ((BYTE)(((float)(grayl < grayscale ? weighting :
                      (weighting ^ 255))) / 255.0 * (rl - rb) + rb)));
            
            BYTE gr = (gb > gl ? ((BYTE)(((float)(grayl < grayscale ?
                            weighting : (weighting ^ 255)))
                      / 255.0 * (gb - gl) + gl)) :
                       ((BYTE)(((float)(grayl < grayscale ? weighting :
                      (weighting ^ 255))) / 255.0 * (gl - gb) + gb)));
            
            BYTE br = (bb > bl ? ((BYTE)(((float)(grayl < grayscale ?
                            weighting : (weighting ^ 255)))
                      / 255.0 * (bb - bl) + bl)) :
                       ((BYTE)(((float)(grayl < grayscale ? weighting :
                      (weighting ^ 255))) / 255.0 * (bl - bb) + bb)));
            
            SET_IMAGE_PIXEL(x0, y0, RGB(rr, gr, br));

            backgroundColor = GET_IMAGE_PIXEL(x0 + dirX, y0);
            
            rb = GETR(backgroundColor);
            gb = GETG(backgroundColor);
            bb = GETB(backgroundColor);
            
            grayscale = rb * 0.299f + gb * 0.587f + bb * 0.114f;
            
            rr = (rb > rl ? ((BYTE)(((float)(grayl < grayscale ?
                        (weighting ^ 255) : weighting))
                 / 255.0 * (rb - rl) + rl)) :
                  ((BYTE)(((float)(grayl < grayscale ? (weighting ^ 255) :
                 weighting)) / 255.0 * (rl - rb) + rb)));
            
            gr = (gb > gl ? ((BYTE)(((float)(grayl < grayscale ?
                        (weighting ^ 255) : weighting))
                 / 255.0 * (gb - gl) + gl)) :
                  ((BYTE)(((float)(grayl < grayscale ? (weighting ^ 255) :
                 weighting)) / 255.0 * (gl - gb) + gb)));
            
            br = (bb > bl ? ((BYTE)(((float)(grayl < grayscale ?
                    (weighting ^ 255) : weighting))
                 / 255.0 * (bb - bl) + bl)) :
                  ((BYTE)(((float)(grayl < grayscale ? (weighting ^ 255) :
                 weighting)) / 255.0 * (bl - bb) + bb)));
            
            SET_IMAGE_PIXEL(x0 + dirX, y0, RGB(rr, gr, br));

        }
        
        SET_IMAGE_PIXEL(x1, y1, color);
        
        return;
    }

    error = ((unsigned long) deltaY << 16) / (unsigned long) deltaX;

    while (deltaX--)
    {
        accumulatedErrorTemp = accumulatedError;
        accumulatedError += error;
        
        if (accumulatedError <= accumulatedErrorTemp)
            y0++;
        
        x0 += dirX;
        
        weighting = accumulatedError >> 8;
        
        assert(weighting < 256);
        assert((weighting ^ 255) < 256);
        
        int32_t bakcgoundColor = GET_IMAGE_PIXEL(x0, y0);
        
        BYTE rb = GETR(bakcgoundColor);
        BYTE gb = GETG(bakcgoundColor);
        BYTE bb = GETB(bakcgoundColor);
        
        float grayscaleBackgound = rb * 0.299f + gb * 0.587f + bb * 0.114f;
        
        BYTE rr = (rb > rl ? ((BYTE)(((float)(grayl < grayscaleBackgound ?
                        weighting:(weighting ^ 255)))
                  / 255.0 * (rb - rl) + rl)) :
                   ((BYTE)(((float)(grayl < grayscaleBackgound ? weighting :
                  (weighting ^ 255))) / 255.0 * (rl - rb) + rb)));
        
        BYTE gr = (gb > gl ? ((BYTE)(((float)(grayl < grayscaleBackgound ?
                        weighting:(weighting ^ 255)))
                  / 255.0 * (gb - gl) + gl)) :
                   ((BYTE)(((float)(grayl < grayscaleBackgound ? weighting :
                  (weighting ^ 255))) / 255.0 * (gl - gb) + gb)));
        
        BYTE br = (bb > bl ? ((BYTE)(((float)(grayl < grayscaleBackgound ?
                        weighting:(weighting ^ 255)))
                  / 255.0 * (bb - bl) + bl)) :
                   ((BYTE)(((float)(grayl < grayscaleBackgound ? weighting :
                  (weighting ^ 255))) / 255.0 * (bl - bb) + bb)));
        
        SET_IMAGE_PIXEL(x0, y0, RGB(rr, gr, br));
        
        bakcgoundColor = GET_IMAGE_PIXEL(x0, y0 + 1);
        
        rb = GETR(bakcgoundColor);
        gb = GETG(bakcgoundColor);
        bb = GETB(bakcgoundColor);
        
        grayscaleBackgound = rb * 0.299f + gb * 0.587f + bb * 0.114f;
        
        rr = (rb > rl ? ((BYTE)(((float)(grayl < grayscaleBackgound ?
                    (weighting ^ 255) : weighting))
             / 255.0 * (rb - rl) + rl)) :
              ((BYTE)(((float)(grayl < grayscaleBackgound ? (weighting ^ 255) :
             weighting)) / 255.0 * (rl - rb) + rb)));
        
        gr = (gb > gl ? ((BYTE)(((float)(grayl < grayscaleBackgound ?
                    (weighting ^ 255) : weighting))
             / 255.0 * (gb - gl) + gl)) :
              ((BYTE)(((float)(grayl < grayscaleBackgound ? (weighting ^ 255) :
             weighting)) / 255.0 * (gl - gb) + gb)));
        
        br = (bb > bl ? ((BYTE)(((float)(grayl < grayscaleBackgound ?
                    (weighting ^ 255) : weighting))
             / 255.0 * (bb - bl) + bl)) :
              ((BYTE)(((float)(grayl < grayscaleBackgound ? (weighting ^ 255) :
             weighting)) / 255.0 * (bl - bb) + bb)));
        
        SET_IMAGE_PIXEL(x0, y0 + 1, RGB(rr, gr, br));
    }
    
    SET_IMAGE_PIXEL(x1, y1, color);
}

void fillConnectPoint(Image *image, int cx, int cy, int x, int y,
                      int crColor, double r)
{
    int backGround;
    int bytesPerLine = image->imageWidth * 4;
    
    if(r < 0) r = 0;
    if(r > 1) r = 1;
    
    backGround = GET_IMAGE_PIXEL(cx + x, cy + y);
    SET_IMAGE_PIXEL(cx + x, cy + y,
                    LINEAR_FILTER_COLOR(crColor, backGround, r));
    backGround = GET_IMAGE_PIXEL(cx + y, cy + x);
    SET_IMAGE_PIXEL(cx + y, cy + x,
                    LINEAR_FILTER_COLOR(crColor, backGround, r));
    backGround = GET_IMAGE_PIXEL(cx + y, cy - x);
    SET_IMAGE_PIXEL(cx + y, cy - x,
                    LINEAR_FILTER_COLOR(crColor, backGround, r));
    backGround = GET_IMAGE_PIXEL(cx + x, cy - y);
    SET_IMAGE_PIXEL(cx + x, cy - y,
                    LINEAR_FILTER_COLOR(crColor, backGround, r));
    backGround = GET_IMAGE_PIXEL(cx - x, cy - y);
    SET_IMAGE_PIXEL(cx - x, cy - y,
                    LINEAR_FILTER_COLOR(crColor, backGround, r));
    backGround = GET_IMAGE_PIXEL(cx - y, cy - x);
    SET_IMAGE_PIXEL(cx - y, cy - x,
                    LINEAR_FILTER_COLOR(crColor, backGround, r));
    backGround = GET_IMAGE_PIXEL(cx - y, cy + x);
    SET_IMAGE_PIXEL(cx - y, cy + x,
                    LINEAR_FILTER_COLOR(crColor, backGround, r));
    backGround = GET_IMAGE_PIXEL(cx - x, cy + y);
    SET_IMAGE_PIXEL(cx - x, cy + y,
                    LINEAR_FILTER_COLOR(crColor, backGround, r));
}

void fillPixels(Image *image, int cx,int cy,int x,int y,int crColor,double r)
{
    int backGround;
    int bytesPerLine = image->imageWidth * 4;
    
    r = (r < 0 ? 0 : 1);
    
    backGround = GET_IMAGE_PIXEL(cx + x + 1, cy + y);
    SET_IMAGE_PIXEL(cx + x + 1, cy + y,
                    LINEAR_FILTER_COLOR(crColor, backGround, r));
    backGround = GET_IMAGE_PIXEL(cx + x, cy + y);
    SET_IMAGE_PIXEL(cx + x, cy + y,
                    LINEAR_FILTER_COLOR(crColor, backGround, r));
    backGround = GET_IMAGE_PIXEL(cx + x + 1, cy + y - 1);
    SET_IMAGE_PIXEL(cx + x + 1, cy + y - 1,
                    LINEAR_FILTER_COLOR(crColor, backGround, r));

    backGround = GET_IMAGE_PIXEL(cx - x - 1, cy + y);
    SET_IMAGE_PIXEL(cx - x - 1, cy + y,
                    LINEAR_FILTER_COLOR(crColor, backGround, r));
    backGround = GET_IMAGE_PIXEL(cx - x, cy + y);
    SET_IMAGE_PIXEL(cx - x, cy + y,
                    LINEAR_FILTER_COLOR(crColor, backGround, r));
    backGround = GET_IMAGE_PIXEL(cx - x - 1, cy + y - 1);
    SET_IMAGE_PIXEL(cx - x - 1, cy + y - 1,
                    LINEAR_FILTER_COLOR(crColor, backGround, r));

    backGround = GET_IMAGE_PIXEL(cx + x + 1, cy - y);
    SET_IMAGE_PIXEL(cx + x + 1, cy - y,
                    LINEAR_FILTER_COLOR(crColor, backGround, r));
    backGround = GET_IMAGE_PIXEL(cx + x, cy - y);
    SET_IMAGE_PIXEL(cx + x, cy - y,
                    LINEAR_FILTER_COLOR(crColor, backGround, r));
    backGround = GET_IMAGE_PIXEL(cx + x + 1, cy - y + 1);
    SET_IMAGE_PIXEL(cx + x + 1, cy - y + 1,
                    LINEAR_FILTER_COLOR(crColor, backGround, r));

    backGround = GET_IMAGE_PIXEL(cx - x - 1, cy - y);
    SET_IMAGE_PIXEL(cx - x - 1, cy - y,
                    LINEAR_FILTER_COLOR(crColor, backGround, r));
    backGround = GET_IMAGE_PIXEL(cx - x, cy - y);
    SET_IMAGE_PIXEL(cx - x, cy - y,
                    LINEAR_FILTER_COLOR(crColor, backGround, r));
    backGround = GET_IMAGE_PIXEL(cx - x - 1, cy - y + 1);
    SET_IMAGE_PIXEL(cx - x - 1, cy - y + 1,
                    LINEAR_FILTER_COLOR(crColor, backGround, r));
}

void drawCircle(Image *image, int cx,int cy,int r,int value)
{
    int bytesPerLine = image->imageWidth * 4;
    
    int    x   = 0;
    int    y   = r;
    int    d   = 1 - r;
    int    dError  = 3;
    int    dsError = -(r << 1) + 5;
    int    ds  = 0;
    float a   = 1.0 / ((r << 1) * 1.4142);
    float temp;
    int backGround;
    
    fillConnectPoint(image, cx, cy, x, y, value, 1.0f);
    fillConnectPoint(image, cx, cy, x, y + 1, value, 1 - 1.2f / 1.5f);
    fillConnectPoint(image, cx, cy, x, y - 1, value, 1 - 1.2f / 1.5f);
    
    while (x < y - 3)
    {
        if (d < 0)
        {
            d += dError;
            dError  += 2;
            dsError += 2;
        }
        else
        {
            ds += 1 - (y << 1);
            d  += dsError;
            dError  += 2;
            dsError += 4;
            y--;
        }
        
        ds += 1 + (x << 1);
        x++;
        
        fillConnectPoint(image, cx, cy, x, y, value, 1 - a * fabs(ds));
        fillConnectPoint(image, cx, cy, x, y + 1, value,
                         1 - a * (ds + (y << 1) + 1));
        fillConnectPoint(image, cx, cy, x, y - 1, value,
                         1 + a * (ds - (y << 1) + 1));
    }

    if (d > -1)
    {
        ds += 1 - (y << 1);
        y--;
    }
    
    ds += 1 + (x << 1);
    x++;
    
    if (x == y)
    {
        temp = 1.0 - a * fabs(ds);
        backGround   = GET_IMAGE_PIXEL(cx + x, cy + y);
        SET_IMAGE_PIXEL(cx + x, cy + y,
                        LINEAR_FILTER_COLOR(value, backGround, temp));
        backGround = GET_IMAGE_PIXEL(cx - x, cy + y);
        SET_IMAGE_PIXEL(cx - x, cy + y,
                        LINEAR_FILTER_COLOR(value, backGround, temp));
        backGround = GET_IMAGE_PIXEL(cx - x, cy - y);
        SET_IMAGE_PIXEL(cx - x, cy - y,
                        LINEAR_FILTER_COLOR(value, backGround, temp));
        backGround = GET_IMAGE_PIXEL(cx + x, cy - y);
        SET_IMAGE_PIXEL(cx + x, cy - y,
                        LINEAR_FILTER_COLOR(value, backGround, temp));
        
        fillConnectPoint(image, cx, cy, x, y + 1, value,
                         1 - a * (ds + (y << 1) + 1));
    }
    else
    {
        temp = 1.0 + a * (ds - (y << 1) + 1);
        backGround   = GET_IMAGE_PIXEL(cx + x, cy + x);
        SET_IMAGE_PIXEL(cx + x, cy + x,
                        LINEAR_FILTER_COLOR(value, backGround, temp));
        backGround = GET_IMAGE_PIXEL(cx - x, cy + x);
        SET_IMAGE_PIXEL(cx - x, cy + x,
                        LINEAR_FILTER_COLOR(value, backGround, temp));
        backGround = GET_IMAGE_PIXEL(cx - x, cy - x);
        SET_IMAGE_PIXEL(cx - x, cy - x,
                        LINEAR_FILTER_COLOR(value, backGround, temp));
        backGround = GET_IMAGE_PIXEL(cx + x, cy - x);
        SET_IMAGE_PIXEL(cx + x, cy - x,
                        LINEAR_FILTER_COLOR(value, backGround, temp));
        
        temp = 1.0 - a * (ds + (x << 1) + 1);
        backGround   = GET_IMAGE_PIXEL(cx + y, cy + y);
        SET_IMAGE_PIXEL(cx + y, cy + y,
                        LINEAR_FILTER_COLOR(value, backGround, temp));
        backGround = GET_IMAGE_PIXEL(cx + y, cy - y);
        SET_IMAGE_PIXEL(cx + y, cy - y,
                        LINEAR_FILTER_COLOR(value, backGround, temp));
        backGround = GET_IMAGE_PIXEL(cx - y, cy - y);
        SET_IMAGE_PIXEL(cx - y, cy - y,
                        LINEAR_FILTER_COLOR(value, backGround, temp));
        backGround = GET_IMAGE_PIXEL(cx - y, cy + y);
        SET_IMAGE_PIXEL(cx - y, cy + y,
                        LINEAR_FILTER_COLOR(value, backGround, temp));
        
        fillConnectPoint(image, cx, cy, x, y, value, 1 - a * fabs(ds));
        fillConnectPoint(image, cx, cy, x, y + 1, value,
                         1 - a * (ds + (y << 1) + 1));
        fillPixels(image, cx, cy, x, y - 1, value, 1 - a * fabs(ds));
    }
}
