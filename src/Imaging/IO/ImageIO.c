/*
 * Copyright 2013, 2014 Li-Yuchong <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <assert.h>

#include "ImageIO.h"

static uint8_t headerFormatJPEG[] = {0xFF, 0xD8, 0xFF};
static uint8_t headerFormatPNG[]  = {0x89, 0x50, 0x4E, 0x47};
static uint8_t headerFormatTIFF[] = {0x49, 0x49, 0x2A, 0x00};

static uint8_t checkCharArrayHit(const uint8_t *mask, const uint8_t *target,
                                 const size_t length)
{
    size_t i;
    uint8_t hit = 1;
    
    for(i = 0; i < length; i++)
        if(mask[i] != target[i])
            hit = 0;
    
    return hit;
}

static enum IMAGE_FILE_FORMAT checkFileFormat(const char *fileName)
{
    FILE *file;
    uint8_t header[32];
    
    file = fopen(fileName, "rb");
    if(!file)
    {
        fprintf(stderr, "cannot open file %s, exit.\n", fileName);
        exit(1);
    }
    
    fread(header, sizeof(uint8_t), 32, file);
    fclose(file);
        
    if(checkCharArrayHit(headerFormatJPEG, header, 3))
        return IMAGE_FORMAT_JPEG;
    if(checkCharArrayHit(headerFormatPNG,  header, 4))
        return IMAGE_FORMAT_PNG;
    if(checkCharArrayHit(headerFormatTIFF, header, 4))
        return IMAGE_FORMAT_TIFF;
    
    return IMAGE_FORMAT_UNKNOWN;
}

int encodeToFileFromImage(const char *fileName, const Image *image,
                          const enum IMAGE_FILE_FORMAT format)
{
    switch (format)
    {
        case IMAGE_FORMAT_JPEG:
            assert(!"sorry, not implemented");
            
        case IMAGE_FORMAT_PNG:
            return encodeToFileFromImagePNG(fileName, image);
            
        case IMAGE_FORMAT_TIFF:
            return encodeToFileFromImageTIFF(fileName, image);
            
        case IMAGE_FORMAT_UNKNOWN:
            fprintf(stderr, "you cannot save an image as \"unknown format\"");
            return -1;
            
        default:
            fprintf(stderr, "unknwon file type");
            return -1;
    }
}

Image *decodeFromFile(const char *fileName)
{
    switch (checkFileFormat(fileName))
    {
        case IMAGE_FORMAT_TIFF:
            assert(!"sorry, not implemented");
            
        case IMAGE_FORMAT_PNG:
            return decodeFromFilePNG(fileName);
            
        case IMAGE_FORMAT_JPEG:
            return decodeFromFileJPEG(fileName);
            
        default:
            assert(!"unknwon format in decodeFromFile()");
            return NULL;
    }
}
