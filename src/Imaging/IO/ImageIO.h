/*
 * Copyright 2013, 2014 Li-Yuchong <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef IMAGE_IO_H
#define IMAGE_IO_H

#include "Image.h"

/* libpng 1.2 */
#include "png.h"

/* libjpeg */
#include <jpeglib.h>
#include <jerror.h>

enum IMAGE_FILE_FORMAT
{
    IMAGE_FORMAT_TIFF,
    IMAGE_FORMAT_PNG,
    IMAGE_FORMAT_JPEG,
    
    IMAGE_FORMAT_UNKNOWN
};

/* ImageIO_PNG.h */
Image *decodeFromFilePNG(const char *fileName);
int    encodeToFilePNG(const char *fileName, const int w, const int h,
                       const uint8_t *rgba);
int    encodeToFileFromImagePNG(const char *fileName, const Image *image);

/* ImageIO_JPEG.c */
Image *decodeFromFileJPEG(const char *fileName);

/* ImageIO_TIFF.c */
int encodeToFileFromImageTIFF(const char *fileName, const Image *image);

/* ImageIO.c */
int    encodeToFileFromImage(const char *fileName,const  Image *image,
                             const enum IMAGE_FILE_FORMAT format);
Image *decodeFromFile(const char *fileName);

#endif /* IMAGE_IO_H */
