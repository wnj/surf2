/*
 * Copyright 2013, 2014 Li-Yuchong <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <assert.h>
#include <stdio.h>

#include "ImageIO.h"
#include "Memory.h"

Image *decodeFromFilePNG(const char *fileName)
{
    char        png_header[8];
    png_structp png_ptr;
    png_infop   info_ptr;
    int         width, height;
    
    FILE *file = fopen(fileName, "rb");
    
    fread(png_header, 1, 8, file);
    if (png_sig_cmp((png_bytep)png_header, 0, 8))
    {
        assert(!"error: not a PNG file\r\n");
        fclose(file);
    }
    
    png_ptr  = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    info_ptr = png_create_info_struct(png_ptr);
    if (setjmp(png_jmpbuf(png_ptr)))
    {
        png_destroy_read_struct(&png_ptr, &info_ptr, png_infopp_NULL);
        fclose(file);
    }

    png_init_io(png_ptr, file);
    png_set_sig_bytes(png_ptr, 8);
    
    png_read_png(png_ptr, info_ptr, PNG_TRANSFORM_EXPAND, 0);
    
    width  = (int)info_ptr->width;
    height = (int)info_ptr->height;
    unsigned char *rgba = (unsigned char *)malloc(width * height * 4 * sizeof(unsigned char));
    MALLOC_FAIL_CHECK(rgba);
    
    png_bytep *row_pointers = png_get_rows(png_ptr, info_ptr);
    
    int pos = 0;
        
    /* longer but faster */
    if (info_ptr->color_type == 2)
    {
        for (int row = 0; row < height; row++)
        {
            for (int col = 0; col < (3 * width); col += 3)
            {
                rgba[pos++] = row_pointers[row][col];
                rgba[pos++] = row_pointers[row][col + 1];
                rgba[pos++] = row_pointers[row][col + 2];
                rgba[pos++] = 255;
            }
        }
    }
    else if (info_ptr->color_type == 6)
    {
        for (int row = 0; row < height; row++)
        {
            for (int col = 0; col < (4 * width); col += 4)
            {
                rgba[pos++] = row_pointers[row][col];
                rgba[pos++] = row_pointers[row][col + 1];
                rgba[pos++] = row_pointers[row][col + 2];
                rgba[pos++] = row_pointers[row][col + 3];
            }
        }
    }
    else if (info_ptr->color_type == 4)
    {
        for (int row = 0; row < height; row++)
        {
            for (int col = 0; col < width * 2; col += 2)
            {
                rgba[pos++] = row_pointers[row][col];
                rgba[pos++] = row_pointers[row][col];
                rgba[pos++] = row_pointers[row][col];
                rgba[pos++] = row_pointers[row][col + 1];
            }
        }
    }
    else
    {
        assert(!"unknown color type");
    }
    
    Image *imageInfo = (Image *)malloc(sizeof(Image));
    MALLOC_FAIL_CHECK(imageInfo);
    
    imageInfo->pixel   = rgba;
    imageInfo->imageHeight = height;
    imageInfo->imageWidth  = width;
    
    png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
    
    fclose(file);
    free(rgba);
    rgba = NULL;
    
    return imageInfo;
}

int encodeToFilePNG(const char *fileName, const int w, const int h, const unsigned char *rgba)
{
    int      i, j, temp;
    png_byte color_type;
    
    png_structp png_ptr;
    png_infop   info_ptr;
    png_bytep   *rowPointers;
    
    /* create file */
    FILE *fp = fopen(fileName, "wb");
    
    if (!fp)
        return -1;
    
    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    
    if (!png_ptr)
        return -1;

    info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr)
        return -1;
    
    if (setjmp(png_jmpbuf(png_ptr)))
        return -1;
    
    png_init_io(png_ptr, fp);
    
    if (setjmp(png_jmpbuf(png_ptr)))
        return -1;
    
    color_type = PNG_COLOR_TYPE_RGB_ALPHA;
    png_set_IHDR(png_ptr, info_ptr, w, h, 8, color_type, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
    png_write_info(png_ptr, info_ptr);
    
    /* write bytes */
    if (setjmp(png_jmpbuf(png_ptr)))
        return -1;

    temp = 4 * w;
    
    rowPointers = (png_bytep *)malloc(h * sizeof(png_bytep));
    MALLOC_FAIL_CHECK(rowPointers);
    
    for (i = 0; i < h; i++)
    {
        rowPointers[i] = (png_bytep)malloc(sizeof(unsigned char) * temp);
        MALLOC_FAIL_CHECK(rowPointers);
        
        for (j = 0; j < temp; j += 4)
        {
            rowPointers[i][j]     = rgba[i * w * 4 + j];
            rowPointers[i][j + 1] = rgba[i * w * 4 + j + 1];
            rowPointers[i][j + 2] = rgba[i * w * 4 + j + 2];
            rowPointers[i][j + 3] = rgba[i * w * 4 + j + 3];
        }
    }
    
    png_write_image(png_ptr, rowPointers);
    
    if (setjmp(png_jmpbuf(png_ptr)))
        return -1;
    
    png_write_end(png_ptr, NULL);
    
    for (j = 0; j < h; j++)
        free(rowPointers[j]);
    
    free(rowPointers);
    
    fclose(fp);
    return 1;
}

int encodeToFileFromImagePNG(const char *fileName, const Image *image)
{
    return encodeToFilePNG(fileName, image->imageWidth, image->imageHeight, image->pixel);
}

