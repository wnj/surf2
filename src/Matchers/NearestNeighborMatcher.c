/*
 * Copyright 2013, 2014 Li-Yuchong <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <float.h>

#include "Matchers.h"

float calculateDistance(const float *vectorsA, const float *vectorsB,
                        const int dimension)
{
    register int i;
    float sum = 0.0f;
    float diff = 0.0f;
    
    for(i = 0; i < dimension; i++)
    {
        diff = vectorsA[i] - vectorsB[i];
        sum += (diff * diff);
    }
    
    return sqrt(sum);
}

MatchPointsVector *NearestNeighborMatch(const FeaturePointsVector *featuresA,
                                        const FeaturePointsVector *featuresB)
{
    int i, j;
    float dist, distance1, distance2;
    
    Match candidateMatch;
    MatchPointsVector *matches;
    
    size_t featurePointsALength = SIMPLE_VECTOR_SIZE(featuresA);
    size_t featurePointsBLength = SIMPLE_VECTOR_SIZE(featuresB);
    
    /* alloc mathces memory, initial size 64 */
    SIMPLE_VECTOR_ALLOC(MatchPointsVector, matches, 64, Match);
    
    for(i = 0; i < featurePointsALength; i++)
    {
        distance1 = distance2 = FLT_MAX;
        
        for(j = 0; j < featurePointsBLength; j++)
        {
            dist = calculateDistance(SIMPLE_VECTOR_AT(featuresA, i)->descriptor,
                                     SIMPLE_VECTOR_AT(featuresB, j)->descriptor,
                                     64);
            
            if(dist < distance1)
            {
                distance2 = distance1;
                distance1 = dist;
                candidateMatch.featurePointA = SIMPLE_VECTOR_AT(featuresB, j);
            }
            else if(dist < distance2)
            {
                distance2 = dist;
            }
        }
        
        if(distance1 / distance2 < 0.65f)
        {
            candidateMatch.featurePointB = SIMPLE_VECTOR_AT(featuresA, i);
            candidateMatch.distance = dist;
            candidateMatch.status = MATCH_STATUS_NORMAL;
            
            SIMPLE_VECTOR_PUSH_BACK(matches, candidateMatch, Match);
        }
    }
    
    return matches;
}