/*
 * Copyright 2013, 2014 Li-Yuchong <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "SURFPrototpyes.h"

struct FeaturePointsVector *extractFeaturePoints(HessianMatrix *hessian)
{
    ResponseLayer *targetResponseLayer;
    
    int octaveFilters, responseMapPos1, responseMapPos2 = 0;
    int tmp, i, row, column;
    
    SIMPLE_VECTOR_ALLOC(FeaturePointsVector,
                        hessian->featurePoints, 32, FeaturePoint *);
    
    buildResponseMap(hessian);
    
    for (octaveFilters = 0;
         octaveFilters < hessian->octaveFilters; octaveFilters++)
    {
        for (i = 0; i < 2; i++)
        {
            tmp = 2 * (octaveFilters + i) - 1;
            responseMapPos1 = (tmp > 0 ? tmp : 0);
            
            targetResponseLayer = &SIMPLE_VECTOR_AT(hessian->responseMap,
                                                    ++responseMapPos2 + 1);
            
            for (row = 0; row < targetResponseLayer->height; row++)
            {
                for (column = 0; column < targetResponseLayer->width; column++)
                {
                    executeInterpolation(hessian, row, column,
                                         targetResponseLayer,
                      &SIMPLE_VECTOR_AT(hessian->responseMap, responseMapPos2),
                      &SIMPLE_VECTOR_AT(hessian->responseMap, responseMapPos1));
                }
            }
        }
    }
    
    /* detection finished, release memory */
    SIMPLE_VECTOR_RELEASE(hessian->responseMap);
    
    return hessian->featurePoints;
}

void insertAllocResponseLayer(HessianMatrix *faseHessian, int width, int height,
                              int sampleCount, int filterSize)
{
    /* alloc ResponseLayer memory */
    ResponseLayer *responseLayerInstance;
    responseLayerInstance =
    responseLayerAlloc(width, height, sampleCount, filterSize);
    
    /* push_back to the vector faseHessian->responseMap */
    SIMPLE_VECTOR_PUSH_BACK(faseHessian->responseMap,
                            *responseLayerInstance, ResponseLayer);
}

void buildResponseMap(HessianMatrix *hessian)
{
    int width = (hessian->integralImage->width / hessian->initSampleCount);
    int height = (hessian->integralImage->height / hessian->initSampleCount);
    int sampleCount = (hessian->initSampleCount);

    int currentFilterSize = 9;
    int currentLayer      = 1;
    int mulCoeff          = 6;
    int scaleFactor       = 1;

	int octaveFilters = hessian->octaveFilters - 1;
    
    int i, j;
    
    /* alloc memory & initialize the vector hessian->responseMap */
    SIMPLE_VECTOR_ALLOC(ResponseLayerVector, hessian->responseMap,
                        32, ResponseLayer);
    
    /* maximum 5 filters */
    if(octaveFilters > 4)
        octaveFilters = 4;
    
	if(octaveFilters >= 1)
	{
        /* insert the first 4 layers */
		for(i = 1; i <= 4; i++)
		{
			insertAllocResponseLayer(hessian, width, height,
                                     sampleCount, currentFilterSize);
			currentFilterSize += currentLayer * mulCoeff;
		}
        
		currentFilterSize -= mulCoeff;
        
        /* insert the remaining layers */
		for(j = currentLayer; currentLayer <= octaveFilters; j++,
            currentLayer++)
		{
			scaleFactor *= 2;
			mulCoeff    *= 2;
            
			currentFilterSize += mulCoeff;
            
            insertAllocResponseLayer(hessian, width / scaleFactor,
                                     height / scaleFactor,
                                     sampleCount * scaleFactor,
                                     currentFilterSize);
			currentFilterSize += mulCoeff;
            insertAllocResponseLayer(hessian, width / scaleFactor,
                                     height / scaleFactor,
                                     sampleCount * scaleFactor,
                                     currentFilterSize);
		}
	}
    
    /* build layers */
    for (i = 0; i < (int)SIMPLE_VECTOR_SIZE(hessian->responseMap); i++)
        buildResponseLayer(hessian, &SIMPLE_VECTOR_AT(hessian->responseMap, i));
}

void buildResponseLayer(const HessianMatrix *hessian,
                        ResponseLayer *responseLayer)
{
    int   aCount, response, current, responseCount, index;
    
    int   filterStep     = responseLayer->filterStep;
    int   border         = (responseLayer->filter - 1) / 2;
    int   lobe           = responseLayer->filter / 3;
    int   filterSize     = responseLayer->filter;
    float normalizeCoeff = 1.0f / (filterSize * filterSize);

    float ddiffX, ddiffY, ddiffXY;
    
    for (responseCount = 0, index = 0;
         responseCount < responseLayer->height; responseCount++)
    {
        for (aCount = 0; aCount < responseLayer->width; aCount++, index++)
        {
            response = responseCount * filterStep;
            current = aCount * filterStep;
            
            ddiffX  = integral(hessian->integralImage,
                               response - lobe + 1, current - border,
                               2 * lobe - 1, filterSize)
                    - integral(hessian->integralImage,
                               response - lobe + 1, current - lobe / 2,
                               2 * lobe - 1, lobe) * 3;
            
            ddiffY  = integral(hessian->integralImage,
                               response - border, current - lobe + 1,
                               filterSize, 2 * lobe - 1)
                    - integral(hessian->integralImage,
                               response - lobe / 2, current - lobe + 1,
                               lobe, 2 * lobe - 1) * 3;
            
            ddiffXY = integral(hessian->integralImage,
                               response - lobe, current + 1, lobe, lobe)
                    + integral(hessian->integralImage,
                               response + 1, current - lobe, lobe, lobe)
                    - integral(hessian->integralImage,
                               response - lobe, current - lobe, lobe, lobe)
                    - integral(hessian->integralImage,
                               response + 1, current + 1, lobe, lobe);
            
            ddiffX  *= normalizeCoeff;
            ddiffY  *= normalizeCoeff;
            ddiffXY *= normalizeCoeff;
            
            responseLayer->responses[index] =
            (ddiffX * ddiffY - 0.81f * ddiffXY * ddiffXY);
            responseLayer->laplacian[index] =
            (uint8_t)(ddiffX + ddiffY >= 0 ? 1 : 0);
        }
    }
}

int executeInterpolation(const HessianMatrix *hessian,
                         int y, int x,
                         ResponseLayer *t, ResponseLayer *m, ResponseLayer *d)
{
    FeaturePoint *featurePoint;
    
    double dx, dy, ds;
    double v, dxx, dyy, dss, dxy, dxs, dys, num;
    
    double Hi[3][3];
    double matrix[3];
    
    float response;

    register int row, cloumn;
    int layerBorder;
    
    layerBorder = (t->filter + 1) / (2 * t->filterStep);
    
    if (y <= layerBorder || y >= t->height - layerBorder
        || x <= layerBorder || x >= t->width - layerBorder)
        return 0;
 
    response = getResponseBetween(m, t, y, x);
 
    /* test threshold */
    if (response < hessian->threshold)
        return 0;
    
    for (row = -1; row <= 1; row++)
    {
        for (cloumn = -1; cloumn <= 1; cloumn++)
        {
            if (getResponse(t, y + row, x + cloumn) >= response
                || ((row != 0 || cloumn != 0) &&
                getResponseBetween(m, t, y + row, x + cloumn) >= response)
                || getResponseBetween(d, t, y + row, x + cloumn) >= response)
                return 0;
        }
    }
    
    /* interpolation */
    dx = (getResponseBetween(m, t, y, x + 1)
          - getResponseBetween(m, t, y, x - 1)) / 2.0f;
    dy = (getResponseBetween(m, t, y + 1, x)
          - getResponseBetween(m, t, y - 1, x)) / 2.0f;
    ds = (getResponse(t, y, x) - getResponseBetween(d, t, y, x)) / 2.0f;
    
    /* calculate hessian matrix */
    v = getResponseBetween(m, t, y, x);
    dxx = getResponseBetween(m, t, y, x + 1)
        + getResponseBetween(m, t, y, x - 1) - 2 * v;
    dyy = getResponseBetween(m, t, y + 1, x)
        + getResponseBetween(m, t, y - 1, x) - 2 * v;
    dss = getResponse(t, y, x) + getResponseBetween(d, t, y, x) - 2 * v;
    dxy = (getResponseBetween(m, t, y + 1, x + 1)
        - getResponseBetween(m, t, y + 1, x - 1)
        - getResponseBetween(m, t, y - 1, x + 1)
        + getResponseBetween(m, t, y - 1, x - 1)) / 4.0f;
    dxs = (getResponse(t, y, x + 1) - getResponse(t, y, x - 1)
        - getResponseBetween(d, t, y, x + 1)
        + getResponseBetween(d, t, y, x - 1)) / 4.0f;
    dys = (getResponse(t, y + 1, x) - getResponse(t, y - 1, x)
        - getResponseBetween(d, t, y + 1, x)
        + getResponseBetween(d, t, y - 1, x)) / 4.0f;
    
    /* pre-evaluation */
    num = ((-dxs) * dyy * dxs + dxy * dys * dxs + dxs * dxy * dys
           - dxx * dys * dys - dxy * dxy * dss + dxx * dyy * dss);
    
    /* calculate inverse matrix */
    Hi[0][0] = ((-dys) * dys + dyy * dss) / num;
    Hi[0][1] = (dxs * dys - dxy * dss) / num;
    Hi[0][2] = ((-dxs) * dyy + dxy * dys) / num;
    Hi[1][0] = (dys * dxs - dxy * dss) / num;
    Hi[1][1] = ((-dxs) * dxs + dxx * dss) / num;
    Hi[1][2] = (dxs * dxy - dxx * dys) / num;
    Hi[2][0] = ((-dyy) * dxs + dxy * dys) / num;
    Hi[2][1] = (dxy * dxs - dxx * dys) / num;
    Hi[2][2] = ((-dxy) * dxy + dxx * dyy) / num;
    
    matrix[0] = (-Hi[0][2]) * ds - Hi[0][0] * dx - Hi[0][1] * dy;
    matrix[1] = (-Hi[1][0]) * dx - dy * Hi[1][1] - ds * Hi[1][2];
    matrix[2] = (-dx) * Hi[2][0] - dy * Hi[2][1] - ds * Hi[2][2]; 
    
    if (fabs(matrix[0]) < 0.5f && fabs(matrix[1])
        < 0.5f && fabs(matrix[2]) < 0.5f)
    {
        featurePoint = featurePointAlloc(64);
        
        featurePoint->x = (float)((x + matrix[0]) * t->filterStep);
        featurePoint->y = (float)((y + matrix[1]) * t->filterStep);
        
        featurePoint->scale =
        (float)((0.13333f) * (m->filter + matrix[2] * (m->filter - d->filter)));
        featurePoint->laplacian = (int)(getLaplacianBetween(m, t, y, x));
        
        SIMPLE_VECTOR_PUSH_BACK(hessian->featurePoints,
                                featurePoint, FeaturePoint *);
    }
    
    return 1;
}
