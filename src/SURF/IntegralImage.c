/*
 * Copyright 2013, 2014 Li-Yuchong <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>

#include "Memory.h"
#include "SURFPrototpyes.h"

void buildIntergralImage(IntegralImage *intergralImage, const Image *image)
{
    /* loop variables */
    register int i, x, y;
    /* number of bytes per line */
    int bytesPerLine;
    
    unsigned char r, g, b;
    float         rowIntegral;
    
    /* copy propeties */
    intergralImage->width  = image->imageWidth;
    intergralImage->height = image->imageHeight;
    
    bytesPerLine = image->imageWidth * 4;
    
    /* alloc memory */
    intergralImage->integration =
    (float **)malloc(sizeof(float *) * (intergralImage->height));
    MALLOC_FAIL_CHECK(intergralImage->integration);
    
    for (i = 0; i < intergralImage->height; i++)
    {
        intergralImage->integration[i] =
        (float *)malloc(sizeof(float) * (intergralImage->width));
        MALLOC_FAIL_CHECK(intergralImage->integration[i]);
    }
    
    /* evaluate the integration of the first row */
    rowIntegral = 0.0f;
    for (x = 0; x < image->imageWidth; x++)
    {
        r = image->pixel[x * 4];
        g = image->pixel[x * 4 + 1];
        b = image->pixel[x * 4 + 2];
        
        rowIntegral += (0.2989f * r + 0.5870f * g + 0.1140f * b) / 255.0f;
        
        intergralImage->integration[0][x] = rowIntegral;
    }
    
    /* evaluate the integration values */
    for (y = 1; y < image->imageHeight; y++)
    {
        rowIntegral = 0.0f;
        for (x = 0; x < image->imageWidth; x++)
        {
            r = image->pixel[bytesPerLine * y + x * 4];
            g = image->pixel[bytesPerLine * y + x * 4 + 1];
            b = image->pixel[bytesPerLine * y + x * 4 + 2];
            
            rowIntegral += (0.2989f * r + 0.5870f * g + 0.1140f * b) / 255.0f;
            
            /* integral image is rowsum + value above */
            intergralImage->integration[y][x] =
            rowIntegral + intergralImage->integration[y - 1][x];
        }
    }
}

void releaseIntergralImage(IntegralImage *intergralImage)
{
    /* clean up */
    register int i;
    
    for (i = 0; i < intergralImage->height; i++)
    {
        free(intergralImage->integration[i]);
        intergralImage->integration[i] = NULL;
    }
    
    free(intergralImage->integration);
    intergralImage->integration = NULL;
}

float integral(const IntegralImage *intergralImage, const int row,
               const int col, const int rows, const int cols)
{
    int r1, c1, r2, c2;
    float A, B, C, D;
    
    r1 = __MIN_(row, intergralImage->height) - 1;
    c1 = __MIN_(col, intergralImage->width) - 1;
    r2 = __MIN_(row + rows, intergralImage->height) - 1;
    c2 = __MIN_(col + cols, intergralImage->width) - 1;
    
    A = ((r1 >= 0) && (c1 >= 0)) ? (intergralImage->integration[r1][c1]):(0.0f);
    B = ((r1 >= 0) && (c2 >= 0)) ? (intergralImage->integration[r1][c2]):(0.0f);
    C = ((r2 >= 0) && (c1 >= 0)) ? (intergralImage->integration[r2][c1]):(0.0f);
    D = ((r2 >= 0) && (c2 >= 0)) ? (intergralImage->integration[r2][c2]):(0.0f);
    
    return __MAX_(0.0f, A - B - C + D);
}

