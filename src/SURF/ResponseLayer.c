/*
 * Copyright 2013, 2014 Li-Yuchong <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "SURFPrototpyes.h"
#include "Memory.h"

ResponseLayer *responseLayerAlloc(const int width, const int height,
                                  const int step, const int filter)
{
    ResponseLayer *responseLayer =
    (ResponseLayer *)malloc(sizeof(ResponseLayer));
    MALLOC_FAIL_CHECK(responseLayer);
    
    /* copy propeties */
    responseLayer->width      = width;
    responseLayer->height     = height;
    responseLayer->filterStep = step;
    responseLayer->filter     = filter;
    
    /* alloc memory */
    responseLayer->responses = (float *)malloc(sizeof(float) * width * height);
    MALLOC_FAIL_CHECK(responseLayer->responses);
    responseLayer->laplacian =
    (unsigned char *)malloc(sizeof(unsigned char) * width * height);
    MALLOC_FAIL_CHECK(responseLayer->laplacian);
    
    /* initialize */
    bzero(responseLayer->responses, sizeof(float) * width * height);
    bzero(responseLayer->laplacian, sizeof(unsigned char) * width * height);
    
    return responseLayer;
}

float getResponse(const ResponseLayer *responseLayer,
                  const int row, const int column)
{
    return responseLayer->responses[row * responseLayer->width + column];
}

unsigned char getLaplacianBetween(const ResponseLayer *this,
                                  const ResponseLayer *responseLayer,
                                  const int row, const int column)
{
    int scale = this->width / responseLayer->width;
    return this->laplacian[(scale * row) * this->width + (scale * column)];
}

float getResponseBetween(const ResponseLayer *this,
                         const ResponseLayer *responseLayer,
                         const int row, const int column)
{
    int scale = this->width / responseLayer->width;
    return this->responses[(scale * row) * this->width + (scale * column)];
}
