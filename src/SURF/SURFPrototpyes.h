/*
 * Copyright 2013, 2014 Li-Yuchong <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SURF_PROTOTYPES_H
#define SURF_PROTOTYPES_H

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

/* imaging */
#include "Image.h"

/* container */
#include "SimpleVector.h"

/* foward declaration */
typedef struct IntegralImage IntegralImage;
typedef struct FeaturePoint  FeaturePoint;
typedef struct ResponseLayer ResponseLayer;
typedef struct HessianMatrix HessianMatrix;

/* declare simple vectors */
SIMPLE_VECTOR_DECLARE(FeaturePointsVector, FeaturePoint *);
SIMPLE_VECTOR_DECLARE(ResponseLayerVector, ResponseLayer);

#define __MIN_(__x, __y) ((__x) > (__y) ? (__y) : (__x))
#define __MAX_(__x, __y) ((__x) < (__y) ? (__y) : (__x))

#define CALCULATE_HARR_RESPONSE_X(__intergralImage, __row, __column, __size)   \
    ((integral(__intergralImage, __row - __size / 2,                           \
        __column, __size, __size / 2) -                                        \
    integral(__intergralImage, __row - __size / 2,                             \
    __column - __size / 2, __size, __size / 2)))

#define CALCULATE_HARR_RESPONSE_Y(__intergralImage, __row, __column, __size)   \
    (integral(__intergralImage, __row, __column - __size / 2,                  \
        __size / 2, __size) -                                                  \
    integral(__intergralImage, __row - __size / 2,                             \
    __column - __size / 2, __size / 2, __size))

#define CALCULATE_GAUSSIAN(__x, __y, __sig)                                    \
    (1.0f / (2.0f * M_PI * __sig * __sig)) * (float)exp(-((__x) *              \
    (__x) + (__y) * (__y)) / (2.0f * __sig * __sig));

struct IntegralImage
{
    float **integration;
    int   width, height;
};

struct FeaturePoint
{
    float x, y;
    float scale;
    float response;
    float orientation;
    int   laplacian;
    int   descriptorLength;
    float *descriptor;
};

struct ResponseLayer
{
    int   width;
    int   height;
    int   filterStep;
    int   filter;
    float *responses;
    unsigned char *laplacian;
};

struct HessianMatrix
{
    /* propeties */
    float threshold;
    int   octaveFilters;
    int   initSampleCount;
    
    /* vectors */
    FeaturePointsVector *featurePoints;
    struct ResponseLayerVector *responseMap;
    
    /* intergral image */
    IntegralImage *integralImage;
};


/* IntegralImage.c */
void  buildIntergralImage   (IntegralImage *intergralImage, const Image *image);
void  releaseIntergralImage (IntegralImage *intergralImage);
float integral              (const IntegralImage *intergralImage,
                             const int row, const int col,
                             const int rows, const int cols);

/* FeaturePoint.c */
FeaturePoint *featurePointAlloc (const int discriptorDimension);
void         releaseFeaturePoint(FeaturePoint *featurePoint);

/* SURFDescriptor.c */
void  calculateFeaturePointsDescriptor(const IntegralImage *integralImage,
                                        FeaturePointsVector *featurePoints,
                                        const size_t startIndex,
                                        const size_t endIndex);
void  calculateOrientation  (const IntegralImage *integralImage,
                             FeaturePoint *featurePoint);
void  calculateDescriptor   (const IntegralImage *integralImage,
                             FeaturePoint *featurePoint);
float calculateAngle        (const float x, const float y);

/* HessainMatrix.c */
void buildResponseMap       (HessianMatrix *hessian);
void buildResponseLayer     (const HessianMatrix *hessian,
                             ResponseLayer *responseLayer);
int  executeInterpolation   (const HessianMatrix *hessian, int row, int column,
                             ResponseLayer *t, ResponseLayer *m,
                             ResponseLayer *d);

/* ResponseLayer.c */
FeaturePointsVector *extractFeaturePoints (HessianMatrix *hessian);
ResponseLayer *responseLayerAlloc(const int width, const int height,
                                  const int step, const int filter);
float getResponse(const ResponseLayer *responseLayer, const int row,
                  const int column);
float getResponseBetween(const ResponseLayer *this,
                         const ResponseLayer *responseLayer,
                         const int row, const int column);
unsigned char getLaplacianBetween(const ResponseLayer *this,
                                  const ResponseLayer *responseLayer,
                                  const int row, const int column);

#endif /* SURF_PROTOTYPES_H */
